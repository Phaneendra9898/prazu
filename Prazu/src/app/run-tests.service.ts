import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RunTestsService {

  constructor(private http: HttpClient) {}
  postDetails(requestObject) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/Json' })
    };
    return this.http.post('http://localhost:40506/api/values', requestObject, httpOptions).subscribe(result => {
        console.log("Result is:",result);
      }, error => console.log('There was an error: '));

  }
}
