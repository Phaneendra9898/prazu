import { Component, OnInit } from '@angular/core';
import { RegisterServiceService } from '../register-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private data: RegisterServiceService) { }

  firstName: string = '';
  lastName: string = '';
  email: string = '';
  password: string = '';

  ngOnInit() {
  }

  handleRegister() {
    var reqObj = {
      'fname': this.firstName,
      'lname': this.lastName,
      'email': this.email,
      'password': this.password
    };
    this.data.fireRequest(reqObj);
  }

}
