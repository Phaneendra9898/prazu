import { Component, OnInit } from '@angular/core';
import { RunTestsService } from '../run-tests.service';

@Component({
  selector: 'app-test-form',
  templateUrl: './test-form.component.html',
  styleUrls: ['./test-form.component.scss']
})
export class TestFormComponent implements OnInit {

  constructor(private data: RunTestsService) { }

  project: string = '';
  release: string = '';
  reqObj: object = {};
  byteArray: Uint8Array[];
  isTCAccepted: boolean = false;
  importedFileName: string = '';

  ngOnInit() {

  }

  onFileUpload(event) {
    console.log(event);
    var files = event.currentTarget.files;
    this.importedFileName = files[0].name;
    var fileData = new Blob([files[0]]);
    var reader = new FileReader();
    reader.readAsArrayBuffer(fileData);

    reader.onload = function () {
      this.byteArray = reader.result;
    }.apply(this);
  }

  onSubmit() {
    this.reqObj = {
      //'NoOftests': this.byteArray,
      'fileName': this.importedFileName,
      'ProjectName': 'test',
      'Browser': 'Chrome',
      'ReleaseName': 'Release',
      'Url': '',
      'Snapshot': 'Snapshot',
      'HostName': '',
      'LocalMachine': 'LocalMachine'
    };
    this.data.postDetails(this.reqObj);

  };
}
